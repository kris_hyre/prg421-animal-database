/*
 * Author: Kris Hyre
 * Class: PRG421 - Java II
 * Date: 08/08/2016
 * Instructor: Roland Morales
 * Description: This project allows the user to list the animals in a database,
 *   list the features of an animal, and add new animals to the database.
 */

package animaldatabase;

import java.sql.*;
import java.util.*;

public class AnimalDatabase {
  
    /*
     * This project assumes the existence of a MySQL database "animal_db" on 
     * the local host with an admin user "project" and a password "Prg421"
     * animal_db has a single table "animals" with the values "idAnimals"
     * (autoincrenmenting), "Name", "Skin", and "Diet"
     */
    
    // Database Variables
    static String url = "jdbc:mysql://localhost/animal_db";
    static String user = "project";
    static String pw = "Prg421";
    static Scanner scanner  = new Scanner(System.in);
        
    public static void main(String[] args) {
    
        // Load DB Driver
        try{
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            System.out.println("Exception: " + ex);
        }
        
        // Menu variables
        Boolean validMenuChoice = false;
        Boolean quitOption = false;
        String menuChoice = null;
        String[] menuOptions = {"a", "A", "l", "L", "S", "s", "e", "E", "d", "D", "q", "Q"};
        
        // Say hello
        System.out.println(greeting);
        
        // Pick a menu option
        while (!quitOption){
            while (!validMenuChoice){
                System.out.println(chooseMenuOption);
                menuChoice = scanner.nextLine();
                for (String menuOption : menuOptions) {
                    if (menuOption.equals(menuChoice)) {
                        validMenuChoice = true;
                        break;
                    }
                }    
                if (!validMenuChoice){
                    System.out.println(invalidChoice);
                }
            }

            // Menu options to add, list, and quit the program
            if  ((menuOptions[0].equals(menuChoice)) || (menuOptions[1].equals(menuChoice))){
                addAnimal();
                validMenuChoice = false;
            } else if ((menuOptions[2].equals(menuChoice)) || (menuOptions[3].equals(menuChoice))){
                getAnimalList();
                validMenuChoice = false;
            } else if ((menuOptions[4].equals(menuChoice)) || (menuOptions[5].equals(menuChoice))){
                getAnimalDetails();
                validMenuChoice = false;
            } else if ((menuOptions[6].equals(menuChoice)) || (menuOptions[7].equals(menuChoice))){
                // Edit Not Yet Implemented
                validMenuChoice = false;
            } else if ((menuOptions[8].equals(menuChoice)) || (menuOptions[9].equals(menuChoice))){                
                // Delete Not Yet Implemented
                validMenuChoice = false;       
            } else if ((menuOptions[10].equals(menuChoice)) || (menuOptions[11].equals(menuChoice))){
                quitOption = true;
            }       
        }
        
        // Goodbye.
        
        System.out.println(quit);
    }
    
    // Shows a list of all animals in the animal_db
    private static void getAnimalList(){
        try{
            Connection conn = DriverManager.getConnection(url, user, pw);
            Statement state = conn.createStatement();
            String testQuery = "SELECT * FROM animals;";
            ResultSet rs = state.executeQuery(testQuery);
            SQLWarning warn = conn.getWarnings();

            while (warn != null){
                System.out.println("SQLState: " + warn.getSQLState());
                System.out.println("Message: " + warn.getMessage());
                warn = warn.getNextWarning();
            }

            while (rs.next()) {
                System.out.print(rs.getInt("idAnimals") + " ");
                System.out.print(rs.getString("Name") + " \n");
            }
            
            // All done? close()
            if ((rs != null) || (state != null)){
                rs.close();
                state.close();
            }
            conn.close();
        } catch (SQLException se){ 
            System.out.println("SQL Exception: " + se);
        } 
        
    }
    
    // Pulls the details from the DB for the specified animal
    private static void getAnimalDetails(){
        System.out.println("Enter the name of the Animal: \n");
        String name = scanner.nextLine();
        try {
            Connection conn = DriverManager.getConnection(url, user, pw);
            Statement state = conn.createStatement();
            String query = "SELECT * FROM animals WHERE Name='" + name + "'";
            ResultSet rs = state.executeQuery(query);
            SQLWarning warn = conn.getWarnings();

            while (warn != null){
                System.out.println("SQLState: " + warn.getSQLState());
                System.out.println("Message: " + warn.getMessage());
                warn = warn.getNextWarning();
            }
            
            while (rs.next()) {
                // System.out.print(rs.getInt("idAnimals") + " ");
                System.out.print("Name: " + rs.getString("Name") + " \n");
                System.out.print("Skin type: " + rs.getString("Skin") + " \n");
                System.out.println("Diet: " + rs.getString("Diet") + " \n");
            }
            
            // All done? close()
            if ((rs != null) || (state != null)){
                rs.close();
                state.close();
            }
            conn.close();
            
        } catch (SQLException se){
            System.out.println("SQL Exception: " + se);
        }
    }
    
    // adds an animla taking input from the user for the three fields
    private static void addAnimal(){
        System.out.println("Enter the name of the animal: \n");
        String name = scanner.nextLine();
        System.out.println("Enter the skin type of the animal: \n");
        String skin = scanner.nextLine();
        System.out.println("Enter the type of diet of the animal (carnivore, herbavore, or omnivore): \n");
        String diet  = scanner.nextLine();
        
        try {
            Connection conn = DriverManager.getConnection(url, user, pw);
            Statement state = conn.createStatement();
            String query = "INSERT INTO animals (Name, Skin, Diet)" + " VALUES ( '" + name + "','" + skin + "','" + diet +"')";
            state.executeUpdate(query);
            SQLWarning warn = conn.getWarnings();

            while (warn != null){
                System.out.println("SQLState: " + warn.getSQLState());
                System.out.println("Message: " + warn.getMessage());
                warn = warn.getNextWarning();
            }
            
            // All done? close()
            if (state != null){
                state.close(); 
            }
            conn.close();
            
        } catch (SQLException se){
            System.out.println("SQL Exception: " + se);
        }
    }
    
    // Strings used for user interface
    static String greeting = "Welcome to the Animal Classification Project. \n";
    static String chooseMenuOption = "Please choose from the following options: \n" +
            "(A)dd an animal. \n" +
            "(L)ist what animals have been added. \n" +
            "(S)how an animal's details \n" +
            "(Q)uit and exit the program. \n";
    static String invalidChoice = "That was not a valid choice. Please choose again.";
    static String quit = "Thank you for using the Animal Classification Project. " +
            "Goodbye.";
}
